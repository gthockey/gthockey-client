# Georgia Tech Hockey Client

This repository holds the frontend code for the Georgia Tech Hockey website that can be found [here](https://www.gthockey.com).

## Client

The client code is contained in the `client/` directory and is written using Angular and Typescript. Once loaded, the entire website is available without page reloads and simply loads data by making API calls to the server.

## Setting up the Client

1. Make sure you have Node downloaded and installed. Either download Node [here](https://nodejs.org/en/) or confirm it is already install by running the following command in your command line
```
$ ng version
```
2. Install the necessary dependencies
```
$ npm install
```
3. Build the client
```
$ ng build
```
4. If the above steps complete without error, you can have angular (ng) serve the client at `localhost:4200` with the following command
```
$ ng serve
```
