# Installing

1. Install NVM
2. Install the latest node: `nvm install node --latest-npm`
3. Install the angular cli: `npm install -g @angular/cli`
3. In the gthockey-client directory run `npm install` to add the rest of the dependencies.
4. Run `ng serve` to start up a development server.