// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiurl: 'http://localhost:8000',
  recaptchaKey: '6Lfltx4TAAAAACF-HzxdwGmJGurEZ6sUcCZ_Bn02',
  paypalClientId: 'AQpJXTQX-ocXqeXXbBM4X-mQXSu4nM5FefibPFfeCttr1PdRj8xZLwqA-dpRbmeNyxghwILT2Y0ynYgt',
  stripePublicKey: 'pk_test_51JS6TvCN8seTnsw1zf7dQPiFUpzFpxwUIyPMnHAWJmtztTI872KmdQBd9brYNJWgtmoPtS2sTSqt3ZcFwwyun9g600rL5k8B6c',
};
