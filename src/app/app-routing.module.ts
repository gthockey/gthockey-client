import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DonationPageComponent } from './pages/donationpage/donationpage.component';
import { ContactFormComponent } from './forms/contact-form/contact-form.component';
import { ProspectFormComponent } from './forms/prospect-form/prospect-form.component';
import { ArticleComponent } from './pages/article/article.component';
import { CartComponent } from './pages/cart/cart.component';
import { FrontpageComponent } from './pages/frontpage/frontpage.component';
import { InvolvementComponent } from './pages/involvement/involvement.component';
import { LeadershipComponent } from './pages/leadership/leadership.component';
import { RosterComponent } from './pages/roster/roster.component';
import { BioComponent } from './pages/bio/bio.component';
import { ScheduleComponent } from './pages/schedule/schedule.component';
import { ShopComponent } from './pages/shop/shop.component';
import { ShopItemComponent } from './pages/shop-item/shop-item.component';
import { SuccessComponent } from './cart/success/success.component';
import { CancelComponent } from './cart/cancel/cancel.component';
import { SponsorshipComponent } from './pages/sponsorships/sponsorship.component';

const routes: Routes = [
  { path: '', component: FrontpageComponent},
  { path: 'sponsorship', component: SponsorshipComponent},
  { path: 'donate', component: DonationPageComponent},
  { path: 'roster', component: RosterComponent },
  { path: 'schedule', component: ScheduleComponent},
  { path: 'board', component: LeadershipComponent},
  { path: 'article/:id', component: ArticleComponent},
  { path: 'roster/:id', component: BioComponent},
  { path: 'contact', component: ContactFormComponent},
  { path: 'prospect', component: ProspectFormComponent},
  { path: 'involvement', component: InvolvementComponent},
  { path: 'shop', component: ShopComponent },
  { path: 'shop/:id', component: ShopItemComponent },
  { path: 'cart', component: CartComponent },
  { path: 'cart/success', component: SuccessComponent },
  { path: 'cart/cancel', component: CancelComponent },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRoutingModule { }
