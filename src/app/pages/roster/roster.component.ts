import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../api/api.service';
import { Player } from '../../api/player';

@Component({
  selector: 'app-roster',
  templateUrl: './roster.component.html',
  styleUrls: ['./roster.component.css']
})
export class RosterComponent implements OnInit {

  positions = new Map<String, Player[]>();

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getPlayers();
  }

  getPlayers() {
    this.apiService.getPlayers().subscribe(players => {
      this.positions.set("Forwards", players.filter(forward => forward.position === 'F'));
      this.positions.set("Defense", players.filter(forward => forward.position === 'D'));
      this.positions.set("Goalies", players.filter(forward => forward.position === 'G'));
      this.positions.set("Managers", players.filter(forward => forward.position === 'M'));
    })
  }

  getPositions(positions){
      return Array.from(positions.keys());
  }

}
