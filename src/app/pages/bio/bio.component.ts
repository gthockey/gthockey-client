import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiService } from '../../api/api.service';
import { Player } from '../../api/player';

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.css']
})
export class BioComponent implements OnInit {

  private id: number;
  player: Player;
  paragraphs: string[];

  constructor(private route: ActivatedRoute,
    private apiService: ApiService) { }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.getPlayer();
  }

  private getPlayer() {
    this.apiService.getPlayers().subscribe(players => {
      this.player = players.find(player => player.id === this.id);
      this.paragraphs = this.player.bio.split('\n');
    });
  }

}