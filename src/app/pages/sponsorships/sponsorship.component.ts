import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RecaptchaComponent } from 'ng-recaptcha';

import { environment } from '../../../environments/environment';
import { ApiService } from '../../api/api.service';
import { InvolvementForm } from '../../api/involvement';

@Component({
  selector: 'app-involvement',
  templateUrl: './sponsorship.component.html',
  styleUrls: ['./sponsorship.component.css']
})
export class SponsorshipComponent implements OnInit {

  model = new InvolvementForm();
  @ViewChild(RecaptchaComponent, { static: true }) recaptcha: RecaptchaComponent;

  success: boolean;
  errors: any = {};

  readonly recaptchaKey = environment.recaptchaKey;

  constructor(private apiService: ApiService, private http: HttpClient) { }

  ngOnInit() {
  }

  submitForm() {
    this.apiService.postInvolvementForm(this.model).subscribe(response => {
      this.success = true;
      this.errors = {};
      this.recaptcha.reset();
      this.model = new InvolvementForm();
    }, response => {
      this.success = false;
      this.errors = response.error.errors;
      this.recaptcha.reset();
    });
  }

  
}
