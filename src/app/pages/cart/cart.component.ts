import { Component, OnInit } from '@angular/core';

import { IPayPalConfig, ICreateOrderRequest, IPurchaseUnit, ITransactionItem } from 'ngx-paypal';

import { CartService } from '../../cart/cart.service';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public payPalConfig ? : IPayPalConfig;

  public showSuccess : boolean;
  public showCancel : boolean;
  public showError : boolean;

  constructor(readonly cartService : CartService) {}

  ngOnInit(): void {
    this.initConfig();
    this.resetStatus();
  }

  private initConfig(): void {
    let items : ITransactionItem[] = [];
    for (let item of this.cartService.allItems()) {
      items.push({
        name: item.getDescription(),
        quantity: '1',
        unit_amount: {
          currency_code: 'USD',
          value: '' + this.cartService.getPrice(item),
        }
      })
    }
    const purchaseUnit : IPurchaseUnit = {
      amount: {
        currency_code: 'USD',
        value: '' + this.cartService.totalCost(),
        breakdown: {
          item_total: {
            currency_code: 'USD',
            value: '' + this.cartService.totalCost(),
          }
        }
      },
      items: items,
    }
    this.payPalConfig = {
      clientId: environment.paypalClientId,
      createOrderOnClient: (data) => < ICreateOrderRequest > {
        intent: 'CAPTURE',
        purchase_units: [purchaseUnit]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'paypal',
        layout: 'vertical'
      },
      onApprove: (data, actions) => {},
      onClientAuthorization: (data) => {
        this.showSuccess = true;
        this.cartService.reset();
      },
      onCancel: (data, actions) => {
        if (!actions['redirect']) {
          this.showCancel = true;
        }
      },
      onError: err => {
        this.showError = true;
      },
      onClick: () => {
        this.resetStatus();
      },
    };
  }

  public cartEmpty() : boolean {
    return this.cartService.empty();
  }

  private resetStatus() {
    this.showSuccess = false;
    this.showCancel = false;
    this.showError = false;
  }
}