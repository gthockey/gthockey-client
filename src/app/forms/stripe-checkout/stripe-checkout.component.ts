import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { ApiService } from '../../api/api.service';
import { CartService } from '../../cart/cart.service';

declare let Stripe: any;

@Component({
  selector: 'app-stripe-checkout',
  templateUrl: './stripe-checkout.component.html',
  styleUrls: ['./stripe-checkout.component.css']
})
export class StripeCheckoutComponent implements OnInit {

  stripe: any;

  constructor(
    private apiService: ApiService,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.stripe = Stripe(environment.stripePublicKey);
  }

  onCheckout(event: any): void {
    var local_stripe = this.stripe;
    this.apiService.createStripeSession(this.cartService.allItems()).pipe(
      map(session => local_stripe.redirectToCheckout({ sessionId: session.id }))).subscribe(
        result => {
          if (result.error) {
            alert(result.error.message);
          }
        },
        err => console.error('Error:', err)
      );
  }

}
