import { getParseErrors } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { ApiService } from '../../api/api.service';
import { Donation } from '../../api/donation';

declare let Stripe: any;

@Component({
  selector: 'app-donation',
  templateUrl: './donation.component.html',
  styleUrls: ['./donation.component.css']
})
export class DonationComponent implements OnInit {
  stripe: any;
  model = new Donation();
  errors: any = {};

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.stripe = Stripe(environment.stripePublicKey);
  }

  submitForm(): void {
    this.errors = {};
    var local_stripe = this.stripe;
    var amount = parseFloat(this.model.amount);
    if (isNaN(amount)) {
      this.errors["amount"] = "Please enter a valid amount";
      return;
    }
    if (amount <= 0.0) {
      this.errors["amount"] = "Please enter a positive amount";
      return;
    }
    this.apiService.createStripeDonation(amount).pipe(
      map(session => local_stripe.redirectToCheckout({ sessionId: session.id }))).subscribe(
        result => {
          if (result.error) {
            alert(result.error.message);
          }
        },
        err => console.error('Error:', err)
      );
  }
}
