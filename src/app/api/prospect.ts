export class ProspectForm {
    name: string;
    email: string;
    phone: string;
    birth: string;
    hometown: string;
    
    status: string = "";
    start: string = "";

    position: string = "";
    handedness: string = "";
    comments: string;
    captcha: string;
}
