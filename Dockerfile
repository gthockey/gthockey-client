# STEP 1 build site
FROM node:10.15.3-alpine as builder
RUN apk update && apk add --no-cache make git
# Create app directory
WORKDIR /app
# Install app dependencies
COPY package.json package-lock.json /app/
RUN cd /app && npm set progress=false && npm install
# Copy project files into the docker image
COPY . /app
RUN cd /app && npm run build:ssr

# STEP 2 Create a minimal serving image
FROM node:10.15.3-alpine

WORKDIR /app

# Copy dependency definitions
COPY --from=builder /app/package.json /app

# Get all the code needed to run the app
COPY --from=builder /app/dist /app/dist

# Expose the port the app runs in
EXPOSE 4000
CMD ["npm", "run", "serve:ssr"]